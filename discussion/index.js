console.log("Hello World");

//Conditional Statements

//Conditional statements allows us to perform task based on a condition

let num1 = 0;

//If statement - if statement allows us to perform a taks IF the condition given is true.

/*
	if(condiiton){
		task/code to perform
	}
	
*/

//example

if(num1 ===0){
	//runs code if the condition is true
	console.log("The value of num1 is 0");
}

num1 = 25

if (num1 === 0) {
	//does not run the code because the condition is false
	console.log("The current value of num1 is still 0");
}

let city = "New York";

if(city === "New Jersey"){
	console.log("Welcome to New Jersey!")
}

//else - executes a code/task if the of previous condition/s are not met.

if(city === "New Jersey"){
	console.log("Welcome to New Jersey!")
}else{
	console.log("This is not New Jersey!")
}


if(num1 < 20){
	console.log("num1's value is less than 20")
}else{
	console.log("num1's value is more than 20s")
}


//Can we use if-else in a function?
//Yes. This improves reusability of our code.

function cityChecker(city){
	if (city === "New York") {
		console.log("Welcome to the empire state!")
	}else{
		console.log("You are not in New York")
	}
}

//invocer
cityChecker("New York");
cityChecker("Los Angeles");

function budget(money){
	if (money<= 40000){
		console.log("You're still within the budget")
	}else{
		console.log("You are currently over budget")
	}
}
budget(50000)
budget(300)

//Else if
//Allows us to execute code/task if the previous condition/s are not met for false and IF the specified condition is met instead.

let city2 = "Manila"

if(city2 === "New Yourk"){
	console.log("Welcome to New York");
}else if(city2 === "Manila"){
	console.log("Welcome to Manila!")
}else{
	console.log("I don't know where you are.")
}

//else and else if statement are optional. Meaning in a conditional chain/statement, there should always be an if statement. You can skip or not add else if or else statement.


// if(city2 === "New York"){
// 	console.log("asdasd")
// }

// else if(city2 === "Manila"){
// 	console.log("I keep coming back to Manila")
// }

// else{
// 	console.log("Where is Waldo")
// }

//Usually we only have a single else statement. Because Else is run when all conditions have not been met.

let role = "admin";

if(role === "developer"){
	console.log("Welcome back, developer");
}else if(role==="admin"){
	console.log("Role is out of bounds")
}else{
	console.log("Role provided is invalid");
}


function determineTyphoonIntensity(windSpeed){

	if(windSpeed < 30){
		return "Not a typhoon yet.";
	}else if(windSpeed <= 61){
		return "Tropical Depression Detected.";
	}else if(windSpeed >=62 && windSpeed <=88){
		return "Tropical Storm Detected.";
	}else if(windSpeed >=89 & windSpeed <= 117){
		return "Severe Tropical Storm Detected";
	}else{
		return "Typhoon Detected"
	}
}



let typhoonMessage1 = determineTyphoonIntensity(29);
let typhoonMessage2 = determineTyphoonIntensity(62);
let typhoonMessage3 = determineTyphoonIntensity(61);
let typhoonMessage4 = determineTyphoonIntensity(88);
let typhoonMessage5 = determineTyphoonIntensity(117);
let typhoonMessage6 = determineTyphoonIntensity(120);

console.log(typhoonMessage1);
console.log(typhoonMessage2);
console.log(typhoonMessage3);
console.log(typhoonMessage4);
console.log(typhoonMessage5);
console.log(typhoonMessage6);

//Truthy and Falsy values

//In JS, there are values that are considered "truthy", which means in a boolean context, like determining an if condition, it is considered true.

//Samples of truthy
//1. true
//2.
if(1){
	console.log("1 is truthy")
}
//3.
if([]){
	//Even though the array is empty, it already exist, it is an existing instance of an Array.
	console.log("[] empty array is truthy")
}

//Falsy values are values considered "false" in a boolean context like determining an if condition

//1. false

//2.
if(0){
	console.log("0 is falsy");
}

//3.
if(undefined){
	console.log("undefined is not falsy")
}else{
	console.log("undefined is falsy")
}

//Conditional Ternary Operator
//Ternary operator is used as a shorter alternative to if else statements
//It is also able to implicitly return a value. Meaning it does not have to use the return keyword to return a value.

//syntax: (condition) ? ifTrue : ifFalse;

let age = 19;
let result = age < 18 ? "underaged" : "Legal age"
console.log(result);

/*let result2 = if(age < 18){
	return "underaged"
}else {
	return "legal age"
}
console.log(result2)*/


//Switch statement
//evaluate an expression and match the expression to a case clause.
//An expression will be compared against different cases. Then, we will be able to run code IF the expression being evaluated matches a case.
//IT is used alternatively from an if-else statement. However, if-else statements provides more complexity in its conditions
//.toLowercase method is a built in JS method which convertes a string to lowercase.

let day = prompt("What day of the week is it today?").toLowerCase();
// console.log(day)

//Switch statement to evaluate the current day and show a message to tell the user the color of the day.
//If the switch statement was not able to match a case with evaluated expression, it will run the default case.

switch(day){
	case "monday":
		console.log("Monday")
		console.log("The color of the day is red.");
		break;
	case "tuesday":
		console.log("Tuesday")
		console.log("The color of the day is orange.");
		break;
	case "wednesday":
		console.log("Wednesday")
		console.log("The color of the day is yellow.");
		break;
	case "thursday":
		console.log("Thursday")
		console.log("The color of the day is green.");
		break;
	case "friday":
		console.log("Friday")
		console.log("The color of the day is blue.");
		break;
	case "saturday":
		console.log("Saturday")
		console.log("The color of the day is black.");
		break;
	case "sunday":
		console.log("Sunday")
		console.log("The color of the day is sunday.");
		break;
	default:
		console.log("Please enter a valid day")
}

//try-catch-finally
//We use the try-catch-finally statements to catch errors, display and inform about the error and continue the code instead of stopping.
//We could also use the try-catch-finally statements to produce our own error message in the event of an error.
//try-catch-finally is used to anticipate, catch and inform about an error.


//try allow us to run code, if there is an error in the instance of our code, then we will be able to catch that error in our catch statement.
try{
	alert("Intensity20")

//the error is passed into the catch statement. Developers usually name the error as error, err or even e.
}catch(error){

	//with the use of the typeof keyword. we will be able to return string which contains information to what data type our error is.
	// console.log(typeof error)
	console.log(error.message)

	//error is an object with methods and properties that describe the error.
	//error.message allows us to access information about the error

	//with a try-catch statement we can catch errors and instead of stopping the whole program, we can catch the error and continue.

	//this is what we call Error Handling
}finally{

	//finally statement will run regardless of the success or failure of the try statement.
	alert("Intensity updates will show in a new alert")
}

console.log("Will we continue to the next code?")