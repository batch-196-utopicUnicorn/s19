console.log("Hello world");
let username = "";
let password = "";
let role = "";

function logIn(){
	username = prompt("Please enter your username")
	if(username === "" || username === null){
		alert("username should not be empty")
	}else{
	alert("Welcome " + username)
	}

	password = prompt("Please enter your password")
	if(password === "" || password === null){
		alert("password should not be empty")
	}else{
	alert("password is correct")
	}

	role = prompt("Please enter your role in this world").toLowerCase()
	if(role === "" || role === null){
		alert("role should not be empty")
	}else{
		switch (role){
			case "admin":
			alert("Welcome back to the class portal, admin " + username);
			break;
			case "teacher":
			alert("Welcome back to the class portal, teacher " + username);
			break;
			case "student":
			alert("Welcome back to the class portal, student " + username);
			break;
			default:
			alert(`Role ${role} for ${username} is out of range`);
		}
	}
}
 
logIn();

//5.

	function grades (number1, number2, number3, number4){
		let average = Math.round((number1 + number2 + number3 + number4)/4);
		
		if(average <= 74){
			console.log(`CheckAverage (${number1},${number2},${number3},${number4})`)
			console.log(`Hello, student. your average is ${average}. The letter equivalent is F`)
		}else if(average >= 74 && average <= 79){
			console.log(`CheckAverage (${number1},${number2},${number3},${number4})`)
			console.log(`Hello, student. your average is ${average}. The letter equivalent is D`)
		}else if(average >= 80 && average <= 84){
			console.log(`CheckAverage (${number1},${number2},${number3},${number4})`)
			console.log(`Hello, student. your average is ${average}. The letter equivalent is C`)
		}else if(average >= 85 && average <= 89){
			console.log(`CheckAverage (${number1},${number2},${number3},${number4})`)
			console.log(`Hello, student. your average is ${average}. The letter equivalent is B`)
		}else if(average >= 90 && average <= 95){
			console.log(`CheckAverage (${number1},${number2},${number3},${number4})`)
			console.log(`Hello, student. your average is ${average}. The letter equivalent is A`)
		}else{
			console.log(`CheckAverage (${number1},${number2},${number3},${number4})`)
			console.log(`Hello, student. your average is ${average}. The letter equivalent is A+`)
		}		

	}

// console.log(grades(69, 71, 76, 72))
// console.log(grades(82, 86, 87, 88))
// console.log(grades(76, 77, 78, 79))
// console.log(grades(82, 83, 81, 85))
// console.log(grades(97, 98, 96, 100))
